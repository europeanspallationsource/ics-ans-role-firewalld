import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('ics-ans-role-firewalld-services')


def test_services_enabled_in_firewall(Sudo, Command):
    with Sudo():
        cmd = Command('firewall-cmd --list-all --permanent | grep services')
    services = cmd.stdout
    for item in ('http', 'rpc-bind'):
        assert item in services
