import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_firewalld_service(Service):
    service = Service('firewalld')
    assert service.is_enabled
    assert service.is_running


def test_ssh_enabled_in_firewall(Sudo, Command):
    with Sudo():
        cmd = Command('firewall-cmd --list-all --permanent | grep services')
    services = cmd.stdout
    assert 'ssh' in services
